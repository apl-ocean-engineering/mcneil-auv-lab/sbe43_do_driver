## odroid_analog_driver

This is a dead-simple ROS node that opens a given ADC pin and reports
the value as an apl_msgs/Adc.msg

It has only been tested using an ODroid C4, but I'd expect it to work on a
Raspberry Pi as well, if the correct version of wiringPi is installed.
For running on an ODroid, install HardKernel's fork of wiringPi.

**Parameters**:
* wiring_pi_pin: The wiringPi pin number to monitor. This will NOT be the physical pin number; instead, use `gpio readall -a` to figure out the mapping.
* rate_hz: Rate to report voltages, in Hz.
* sample_count: How many times to sample the pin. (On an ODroid, an analogRead takes < 80 usec.) Header timestamp will be average of calls to ros::Time::now() just before first sample and just after the last.
* max_voltage (volts): voltage corresponding to maximum ADC counts
* max_counts: maximum value reported by the ADC pin


**Publications**:
* ~/voltage (`apl_msgs/Adc.msg`): Measured voltage, along with raw count values
