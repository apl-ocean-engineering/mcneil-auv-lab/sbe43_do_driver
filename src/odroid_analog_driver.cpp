/**
 * Copyright 2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "odroid_analog_driver/odroid_analog_driver.h"

#include <wiringpi2/wiringPi.h>

#include <numeric>  // for std::reduce

#include "apl_msgs/Adc.h"
#include "ros/ros.h"

OdroidAnalogDriver::OdroidAnalogDriver() {
  nh_ = ros::NodeHandle("~");
  nh_.getParam("wiringpi_pin", wiringpi_pin_);
  nh_.getParam("rate_hz", rate_hz_);
  nh_.getParam("num_samples", num_samples_);
  nh_.getParam("max_counts", max_counts_);
  nh_.getParam("max_voltage", max_voltage_);

  adc_pub_ = nh_.advertise<apl_msgs::Adc>("voltage", 10);

  wiringPiSetup();
}

OdroidAnalogDriver::~OdroidAnalogDriver() = default;

void OdroidAnalogDriver::run() {
  ros::Rate analog_rate = ros::Rate(rate_hz_);
  while (ros::ok()) {
    readPin();
    analog_rate.sleep();
  }
}

void OdroidAnalogDriver::readPin() {
  int measurement;
  std::vector<int> measurements;
  double start_time = ros::Time::now().toSec();
  for (int ii = 0; ii < num_samples_; ii++) {
    measurement = analogRead(wiringpi_pin_);
    measurements.push_back(measurement);
  }
  double end_time = ros::Time::now().toSec();
  double measurement_time = (start_time + end_time) / 2;

  apl_msgs::Adc adc_msg;
  adc_msg.header.stamp = ros::Time(measurement_time);
  double avg_counts = std::reduce(measurements.begin(), measurements.end()) /
                      measurements.size();
  adc_msg.counts.swap(measurements);
  adc_msg.voltage = max_voltage_ * avg_counts / max_counts_;
  adc_msg.max_counts = max_counts_;  // 12-bit ADC
  adc_pub_.publish(adc_msg);
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "odroid_analog_driver");
  OdroidAnalogDriver aa;
  aa.run();
  return 0;
}
